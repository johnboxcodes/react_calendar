import React from 'react';
import './App.sass';
import Calendar from './features/Calendar/Calendar';

function App() {
  return (
    <div>
      <Calendar />
    </div>
  );
}

export default App;
