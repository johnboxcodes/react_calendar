import React, { useState } from 'react'

import CalendarDeliveryDate from './components/CalendarDeliveryDate/CalendarDeliveryDate'
import './Calendar.sass'
import { CalendarContext, ICalendarState } from './context/Calendar.context'
import CalendarModal from './components/CalendarModal/CalendarModal'

export interface ICalendar {

}

const Calendar = () => {

  const [calendarContext, setCalendarContext] = useState<ICalendarState>({
    deliveryDate: new Date().toLocaleDateString('en-GR'),
    openModal: false,
    deliveryDay: 1
  });

  return (
    <CalendarContext.Provider value={[calendarContext, setCalendarContext]}>
      <div className="calendar--wrapper">
        <div className="disclaimers">
          <span className="disclaimers--title">Choose your delivery day</span>
          <span className="disclaimers--message">Delivery is always free</span>
        </div>
        <div className="calendar--button">
          <CalendarDeliveryDate />
        </div>
        {calendarContext.openModal &&
          <>
            <CalendarModal />
          </>
        }
      </div>
    </CalendarContext.Provider>

  )
}

export default Calendar
