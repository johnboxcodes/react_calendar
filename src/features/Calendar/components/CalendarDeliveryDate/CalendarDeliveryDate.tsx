import React, { CSSProperties, useContext, useEffect, useState } from 'react'

import VanIcon from '../../../../van.svg'
import CalendarIcon from '../../../../calendar.svg'
import CutPiece from '../../../../cutpiece.svg'
import ChevronRight from '../../../../chevron-right.svg'
import Bar from '../../../../bar.svg'
import './CalendarDeliveryDate.sass'
import { CalendarContext } from '../../context/Calendar.context'

const CalendarDeliveryDate = () => {

  const [deliveryDay, setDeliveryDay] = useState<string>("")
  const [calendarContext, setCalendarContext] = useContext(CalendarContext)

  const deliveryDateLabel = (deliveryDate: string) => {
    const date = new Date(deliveryDate)
    const options: Intl.DateTimeFormatOptions = {
      weekday: 'short',
      month: 'short',
      day: 'numeric'
    }
    return date.toLocaleDateString('en-GR', options)
  }

  const handleDeliveryDatePosition = (deliveryDate: string): CSSProperties => {
    return deliveryDate.length === 2 ? {
      left: 30,
    } : {
      left: 32,
    }
    // deliveryDate.toString().length === 2 ? {}
  }
  useEffect(() => {
    const _deliveryDay = deliveryDateLabel(calendarContext.deliveryDate).split(' ')[2]
    setDeliveryDay(_deliveryDay)
  }, [calendarContext.deliveryDate])

  return (
    <div className='calendar--delivery-date--wrapper'>
      <div className='calendar--delivery-date--delivery-date'>
        <div className='delivery-date'>
          <span>{deliveryDateLabel(calendarContext.deliveryDate)}</span>
        </div>
        <div className='delivery-date-label'>
          <img className='calendar--delivery-date--icon-van' src={VanIcon} alt='An icon representing a delivery van' />
          <div className='calendar--delivery-date--icon-text'>
            <span>Earliest delivery</span>
          </div>
        </div>
      </div>
      <div className='calendar--delivery-date--change-date'
        onClick={() => setCalendarContext({ ...calendarContext, openModal: !calendarContext.openModal })}
      >
        <img className='calendar--delivery-date--cutpiece' src={CutPiece} alt='An icon representing a delivery van' />
        <img className='calendar--delivery-date--icon-cal' src={CalendarIcon} alt='An icon representing a delivery van' />
        <span className='calendar--delivery-date--day' style={handleDeliveryDatePosition(deliveryDay)}>{deliveryDay}</span>
        <img className='calendar--delivery-date--chevron-right' src={ChevronRight} alt='An icon representing a delivery van' />
        <img className='calendar--delivery-date--bar' src={Bar} alt='An icon representing a delivery van' />
        <span className='calendar--delivery-date--text'>Change</span>
      </div>
    </div>
  )
}

export default CalendarDeliveryDate
