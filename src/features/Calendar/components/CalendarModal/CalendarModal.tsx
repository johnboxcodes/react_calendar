import React, { useContext, useEffect, useRef, useState } from 'react'

import { weekDays } from '../../../../consts/weekdays'
import { getDaysInMonth } from '../../../../helpers/dateUtils'
import { WeekDays } from '../../../../types/WeekDays'
import { CalendarContext } from '../../context/Calendar.context'
import './CalendarModal.sass'

const CalendarModal = () => {
  const [calendarContext, setCalendarContext] = useContext(CalendarContext)
  const currentMonthYear = new Date().toLocaleString('en-GR', { month: 'long', year: 'numeric' })
  const [choosenDay, setChoosenDay] = useState<number>(0)
  const modalRef = useRef<HTMLDivElement>(null)
  const cancelRef = useRef<HTMLDivElement>(null)
  const confirmRef = useRef<HTMLDivElement>(null)
  const currentDate = new Date()
  const currentMonth = currentDate.getMonth() + 1
  const currentYear = currentDate.getFullYear()
  const generateOffset = () => {
    const firstDay = new Date(`${currentYear}-${currentMonth}-${1}`).getDay()
    const offSet = []
    for (let i = 0; i < (firstDay - 1); i++) {
      offSet.push(<div key={i}></div>)
    }
    return offSet
  }

  const calendarHeader = () => {
    return (<div className="calendar-modal--display-grid--header">
      {
        weekDays.map((dayName, index) =>
          <div className="calendar-modal--display-grid--header-col"
            key={dayName + index}><span>{dayName}</span></div>)
      }
    </div>)
  }

  const handleClose = () => {
    modalRef.current?.classList.add('close')
    setTimeout(() => {
      setCalendarContext({ ...calendarContext, openModal: !calendarContext.openModal })
    }, 200)
  }
  const weekDaysClasses = (day: number, choosenDay: number, deliveryDay: number) => {
    let classStr = 'calendar-modal--display-grid--weekday'
    classStr += day === choosenDay ? (' ' + 'choosen-day') : ''
    classStr += day === deliveryDay ? (' ' + 'delivery-day') : ''
    return classStr
  }
  const renderDays = () => {
    const daysInMonth = getDaysInMonth(currentMonth, currentYear)
    return daysInMonth.dates.map((day: number) =>
      <div
        className={weekDaysClasses(day, choosenDay, calendarContext.deliveryDay)}
        onClick={() => setChoosenDay(day)}
        key={day} > {day}</div >)
  }

  const handleMouseDown = (element: HTMLElement | null) => {
    if (element) {
      element.classList.add("active")
    }
  }
  const handleMouseUp = (element: HTMLElement | null) => {
    if (element) {
      element.classList.remove("active")
    }
  }

  const handleNewDeliveryDate = () => {
    modalRef.current?.classList.add('close')
    if (choosenDay) {
      setTimeout(() => {
        setCalendarContext({
          deliveryDate: `${currentMonth}/${choosenDay}/${currentYear}`,
          deliveryDay: choosenDay,
          openModal: false,
        })
      }, 300);
    }
  }

  return (
    <div className="calendar-modal--wrapper">
      <div className="calendar-modal--overlay"
        onClick={handleClose}></div>
      <div ref={modalRef} className="calendar-modal--display">
        <div className="calendar-modal--display--current-month">
          <span>{currentMonthYear}</span>
        </div>
        <div className="calendar-modal--display-weeknames">
          {calendarHeader()}
        </div>
        <div className="calendar-modal--display-grid">
          {generateOffset()}
          {renderDays()}
        </div>
        <div className="calendar-modal--controls">
          <div ref={cancelRef}
            onMouseDown={() => handleMouseDown(cancelRef.current)}
            onMouseUp={() => handleMouseUp(cancelRef.current)}
            onClick={handleClose}
            className="calendar-modal--controls-cancel">
            <span>cancel,</span>
            <span> don't change </span>
          </div>
          <div ref={confirmRef}
            className="calendar-modal--controls-confirm"
            onMouseDown={() => handleMouseDown(confirmRef.current)}
            onMouseUp={() => handleMouseUp(confirmRef.current)}
            onClick={handleNewDeliveryDate}
          >
            <span>change date</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CalendarModal
