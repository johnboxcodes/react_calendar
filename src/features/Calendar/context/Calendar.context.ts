import React from 'react'

export interface ICalendarState {
  deliveryDate: string
  openModal: boolean
  deliveryDay: number
}

type CalendarState = [ICalendarState, (value: ICalendarState) => void];

export const CalendarContext = React.createContext<CalendarState>([
  {
    deliveryDate: "",
    openModal: false,
    deliveryDay: 1
  },
  (value: ICalendarState) => { },
]);