import { getDaysInMonth } from "./dateUtils";



describe("dateUtils test Suite", () => {

  it('should return the correct weekday of first day of month', () => {
    expect(getDaysInMonth(10, 2021).startWeekDay).toEqual("F")
    expect(getDaysInMonth(10, 1990).startWeekDay).toEqual("M")
    expect(getDaysInMonth(7, 2002).startWeekDay).toEqual("M")
    expect(getDaysInMonth(6, 2025).startWeekDay).toEqual("S")
    expect(getDaysInMonth(9, 2054).startWeekDay).toEqual("T")
    expect(getDaysInMonth(1, 2054).startWeekDay).toEqual("T")
    expect(getDaysInMonth(3, 2054).startWeekDay).toEqual("S")
    expect(getDaysInMonth(12, 2054).startWeekDay).toEqual("T")
    expect(getDaysInMonth(3, 3020).startWeekDay).toEqual("W")
    expect(getDaysInMonth(11, 1987).startWeekDay).toEqual("S")
    expect(getDaysInMonth(6, 2012).startWeekDay).toEqual("F")
  })
  it('should return the correct number of first days in a month', () => {
    expect(getDaysInMonth(1, 2021).dates.length).toEqual(31)
    expect(getDaysInMonth(2, 2021).dates.length).toEqual(28)
    expect(getDaysInMonth(3, 2021).dates.length).toEqual(31)
    expect(getDaysInMonth(4, 2021).dates.length).toEqual(30)
    expect(getDaysInMonth(5, 2021).dates.length).toEqual(31)
    expect(getDaysInMonth(6, 2021).dates.length).toEqual(30)
    expect(getDaysInMonth(7, 2021).dates.length).toEqual(31)
    expect(getDaysInMonth(8, 2021).dates.length).toEqual(31)
    expect(getDaysInMonth(9, 2021).dates.length).toEqual(30)
    expect(getDaysInMonth(10, 2021).dates.length).toEqual(31)
    expect(getDaysInMonth(11, 2021).dates.length).toEqual(30)
    expect(getDaysInMonth(12, 2021).dates.length).toEqual(31)

  })
})