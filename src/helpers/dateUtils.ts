import { weekDays } from "../consts/weekdays";
import { WeekDays } from "../types/WeekDays";

// the classic range function from mdn!
const range = (start: number, stop: number, step: number) => Array.from({ length: (stop - start) / step + 1 }, (_, i) => start + (i * step));

const daysInMonth = (month: number, year: number) => {
  return new Date(year, month, 0).getDate();
}

export interface WeeksInMonth {
  dates: number[]
  startWeekDay: WeekDays;
}

export const getDaysInMonth = (month: number, year: number): WeeksInMonth => {
  const firstDay = 1
  const lastDay = daysInMonth(month, year)
  const firstDayDate = new Date(`${year}-${month}-${firstDay}`)
  const firstDayWeekDayNumber = firstDayDate.getDay()
  const firstDayWeekDay = (firstDayWeekDayNumber > 0) ? weekDays[firstDayWeekDayNumber - 1] : "S"

  // TODO get first day of month weekday and add elements
  // to array to create and offset on the grid
  const result: WeeksInMonth = {
    dates: range(firstDay, lastDay, 1),
    startWeekDay: firstDayWeekDay
  }
  return result;
};
